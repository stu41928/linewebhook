var express = require('express');
var router = express.Router();
var https = require ('https');
const ChannelAccessKey = ' ';
const line = require ('@line/bot-sdk');

const client = new line.Client({
  channelAccessToken: ChannelAccessKey
});

router.post('/line', function(req, res, next) {
  console.log(JSON.stringify(req.body, null, 2));
  res.status(200).send();

  let targetEvent = req.body.events[0]; 

  if(targetEvent.type == 'message'){
    client.getProfile(targetEvent.source.userId)
    .then((profile) => {
      replyToLine(targetEvent.replyToken, profile.displayName, targetEvent.message);
    })
  }
});

function replyToLine(replyToken, name, message){
  client.replyMessage(replyToken, {type : "text" , text : name + "說" + message.text})
  .then((result) => {
    console.log('reply message success', result);
  })
  .catch((err) => {
    console.log('reply message fail', err);
  });
};

module.exports = router;
